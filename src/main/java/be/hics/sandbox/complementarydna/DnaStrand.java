package be.hics.sandbox.complementarydna;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * https://www.codewars.com/kata/complementary-dna/java
 * Kata
 * Deoxyribonucleic acid (DNA) is a chemical found in the nucleus of cells and carries the "instructions" for the development and functioning of living organisms.
 * If you want to know more http://en.wikipedia.org/wiki/DNA
 * In DNA strings, symbols "A" and "T" are complements of each other, as "C" and "G". You have function with one side of the DNA (string, except for Haskell); you need to get the other complementary side. DNA strand is never empty or there is no DNA at all (again, except for Haskell).
 * DnaStrand.makeComplement("ATTGC") // return "TAACG"
 * DnaStrand.makeComplement("GTAT") // return "CATA"
 */
public class DnaStrand {

    public static String makeComplement(final String dna) {
        return dna.chars().mapToObj(c -> String.valueOf((char) c)).map(DnaStrand::complement).collect(Collectors.joining());
    }

    private static String complement(final String s) {
        Map<String, String> dnaComplements = new HashMap<>();
        dnaComplements.put("A", "T");
        dnaComplements.put("T", "A");
        dnaComplements.put("G", "C");
        dnaComplements.put("C", "G");
        return dnaComplements.get(s);
    }

}