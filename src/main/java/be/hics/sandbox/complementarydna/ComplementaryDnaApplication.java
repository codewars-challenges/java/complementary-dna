package be.hics.sandbox.complementarydna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplementaryDnaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComplementaryDnaApplication.class, args);
        prettyPrintDnaStrand("TTTT");
        prettyPrintDnaStrand("TAACG");
        prettyPrintDnaStrand("CATA");
    }

    private static void prettyPrintDnaStrand(final String dna) {
        System.out.println(String.format("%s|%s", dna, DnaStrand.makeComplement(dna)));
    }
}
